package com.rdas.grpc.service;

import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
import com.opencsv.bean.util.OpencsvUtils;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;
import com.rdas.grpc.model.CsvTransaction;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Log4j2
@Service
public class TransactionService {
    private List<String[]> rows;

    @PostConstruct
    public void init() throws URISyntaxException, IOException, CsvException {
        Reader reader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource("csv/electronic-card-transactions-june-2020-csv-tables.csv").toURI()));

//        lineByLine(reader);
        getTransactions(reader);
    }

    public List<String[]> readAll(Reader reader) throws IOException, CsvException {
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> list = csvReader.readAll();
        reader.close();
        csvReader.close();
        return list;
    }

    public List<String[]> lineByLine(Reader reader) throws IOException, CsvValidationException {
        List<String[]> list = new ArrayList<>();
//        CSVReader csvReader = new CSVReader(reader);
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
        String[] line;
        while ((line = csvReader.readNext()) != null) {
            list.add(line);
        }
        reader.close();
        csvReader.close();
        return list;
    }

    public List<CsvTransaction> getTransactions(Reader reader) throws IOException {
        /*
        Map mapping = new HashMap();
        mapping.put("Series_Reference ", "seriesReference");
        HeaderColumnNameTranslateMappingStrategy strategy = new HeaderColumnNameTranslateMappingStrategy();
        strategy.setType(CsvTransaction.class);
        strategy.setColumnMapping(mapping);
        CsvToBean csvToBean = new CsvToBean();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                //.withSkipLines(1)
                .build();
        // call the parse method of CsvToBean
        // pass strategy, csvReader to parse method

//        List<CsvTransaction> list = csvToBean.parse(strategy, csvReader);

        CsvToBean<CsvTransaction> bean = new CsvToBean<>();
//        CSVParser parser = buildParser();
        bean.setCsvReader(csvReader);//buildReader(parser));
        //bean.setThrowExceptions(throwExceptions);
        //bean.setOrderedResults(orderedResults);
        //if(filter != null) { bean.setFilter(filter); }
        //bean.setVerifiers(verifiers);
//        mappingStrategy = OpencsvUtils.determineMappingStrategy(type, errorLocale);
        bean.setMappingStrategy(strategy);
//        bean.setErrorLocale(errorLocale);

        Iterator<CsvTransaction> iterator = bean.iterator();
        iterator.forEachRemaining(csvTransaction -> System.out.println(csvTransaction.toString()));
        return null;*/

        CsvTransfer csvTransfer = new CsvTransfer();
        ColumnPositionMappingStrategy ms = new ColumnPositionMappingStrategy();
        ms.setType(CsvTransaction.class);

//        Reader reader = Files.newBufferedReader(path);
        CsvToBean cb = new CsvToBeanBuilder(reader)
                .withType(CsvTransaction.class)
                .withMappingStrategy(ms)
                .build();

        csvTransfer.setCsvList(cb.parse());
        reader.close();

        List<CsvBean> csvList = csvTransfer.getCsvList();
        log.info(csvList);
        return null;
    }
}
