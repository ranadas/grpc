package com.rdas.grpc.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CsvTransaction {
    ////    Series_reference,Period,Data_value,Suppressed,STATUS,UNITS,Magnitude,Subject,Group,Series_title_1,Series_title_2,Series_title_3,Series_title_4,Series_title_5
    @CsvBindByPosition(position = 0)
    private String  seriesReference;

    //@CsvBindByName(column = "Data_value")
    @CsvBindByPosition(position = 2)
    private String dataValue;

    @CsvBindByPosition(position = 4)
    private String status;

    @CsvBindByPosition(position = 5)
    private String units;

    @CsvBindByPosition(position = 7)
    private String subject;

    @CsvBindByPosition(position = 8)
    private String group;

    @CsvBindByPosition(position = 9)
    private String title1;
}
