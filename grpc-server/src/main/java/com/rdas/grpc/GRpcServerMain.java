package com.rdas.grpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GRpcServerMain {
    public static void main(String[] args) {
        SpringApplication.run(GRpcServerMain.class, args);
    }
}
