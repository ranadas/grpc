CREATE TABLE cities
(
    id         BIGINT PRIMARY KEY AUTO_INCREMENT,
    name       VARCHAR2(30),
    population BIGINT
);