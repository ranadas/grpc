**gRPC** is a modern open source high performance RPC framework that can run in any environment. It can efficiently connect services in and across data centers with pluggable support for load balancing, tracing, health checking and authentication. It is also applicable in last mile of distributed computing to connect devices, mobile applications and browsers to backend services.

**gRPC** embraces an open-source technology called Protocol Buffers.

[Google Docs](https://grpc.io/docs/)

[Microsoft Link](https://docs.microsoft.com/en-us/dotnet/architecture/cloud-native/grpc)

[Article on Spring boot gRPC](https://medium.com/@sajeerzeji44/grpc-for-spring-boot-microservices-bd9b79569772)

[gPRC github Code](https://github.com/yrreddy0123/yrrhelp)



 